#!/usr/bin/python2.7

import os, sys
import Image

def resize():
    directories = os.listdir("./")
    directories.remove("players")
    directories.remove("images")
    directories.remove("jquery")
    
    for directory in directories:
        if os.path.isdir(directory):
            if not "mini" in directory:
                directory += "/"
                files = os.listdir(directory)
                
                dir_mini = directory[0:len(directory)-1] + "_mini/"
                files_mini = os.listdir(dir_mini)
                
                if "text.txt" in files:
                    files.remove("text.txt")
                if "Thumbs.db" in files:
                    files.remove("Thumbs.db")

                nb = 0
                to_resize = list()
                for i in range(len(files)):
                    if files[i] not in files_mini:
                        nb += 1
                        to_resize.append(files[i])
                
                if nb > 0:
                    print("Images to resize: {}".format(nb))

                for i, image in enumerate(to_resize):
                    print("Resizing {}/{}".format(i+1, nb))
                    
                    basewidth = 500
                    img = Image.open(directory + image)
                    wpercent = (basewidth / float(img.size[0]))
                    hsize = int((float(img.size[1]) * float(wpercent)))
                    img = img.resize((basewidth, hsize), Image.ANTIALIAS)
                    img.save(dir_mini + image)
                    

if __name__ == "__main__":
    resize()
