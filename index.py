#!/usr/bin/env python2
from jinja2 import Environment, FileSystemLoader
from datetime import datetime
import cgi
import sys
import os

# fucking russian
reload(sys)
sys.setdefaultencoding('utf-8')

# arguments for template
arguments = {}

# Get template
env = Environment(loader=FileSystemLoader('templates'), lstrip_blocks=True,
        trim_blocks=True)
template = env.get_template('index.html')

# Required
print "Content-type: text/html"
print

# Available categories
available = ["vehicles", "weapons", "food", "landscapes", "girls"]

# Get current category
current_cat = cgi.FieldStorage().getvalue("cat")
if not current_cat:
    current_cat = available[0]

# Correct category if not available
GET = cgi.FieldStorage()
if GET.getvalue("cat") in available:
    directory = "./" + GET.getvalue("cat") + "/"
else:
    directory = "./" + available[0] + "/"

# Get modifications dates of files
files = os.listdir(directory)
files.remove('text.txt') if 'text.txt' in files else None

modifs = [(datetime.fromtimestamp(os.path.getmtime(directory + file)), file) 
         for file in files]
modifs = sorted(modifs, reverse=True)
modifs = [(m[0].strftime('%d/%m/%Y %H:%M:%S'), m[1]) for m in modifs]

# Get thumbnails
dir_mini = directory[0:len(directory)-1] + "_mini/"
files_mini = os.listdir(dir_mini)

# Get last dates
last_dates = []
if len(modifs) % 3 == 1 and len(modifs) > 3:
    last_dates.append(modifs[-1][0])
elif len(modifs) % 3 == 3 and len(modifs) > 3:
    last_dates.append(modifs[-1][0])
    last_dates.append(modifs[-2][0])

# Get links
links = []
if 'text.txt' in os.listdir(directory):
    links = open(directory + 'text.txt')

# Build arguments
arguments['categories'] = available # navigation
arguments['current_cat'] = current_cat # navigation
arguments['images'] = modifs # iterate through images
arguments['mini'] = files_mini # thumbnails
arguments['directory'] = directory # images dir
arguments['dir_mini'] = dir_mini # thumbnails dir
arguments['last_dates'] = last_dates # dates if len % 3 != 0
arguments['links'] = links # some sources

print(template.render(**arguments))
